# Installation on Easypanel

## Deploying Baserow on Easypanel

[Easypanel](https://easypanel.io) it's a modern server control panel. You can use it to deploy Baserow on your own server.

[![Deploy to Easypanel](https://easypanel.io/img/deploy-on-easypanel-40.svg)](https://easypanel.io/docs/templates/baserow)

## Instructions

1. Create a VM that runs Ubuntu on your cloud provider.
2. Install Easypanel using the instructions from the website.
3. Create a new project.
4. Install Baserow using the dedicated template.
